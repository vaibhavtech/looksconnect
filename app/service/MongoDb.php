<?php

namespace app\service{

	Class MongoDb {

		protected static $__instance;
	    private $__settings;
	    private $__connected;
	    private $_collection;
	    private $_mng;
	    private $_cmd;
	    private $config;

	    public function __construct(){
	        $this->connect();

	        if($this->__connected===true){
	            return true;
	        }
	        else{
	            return false;
	        }
	    }

	    public function connect(){
	        if($this->__connected) return true;
	        try {
	            /**
	             * Construct Connection String;
	             *
	             */
	            $this->config = \Config::getSection("MONGODB_CONFIG");
	            $connection_string = "mongodb://" . $this->config['host'] . ':' . $this->config['port']. '/' . $this->config['dbname'];

	            /**
	             * Create Connection to the MongoDB
	             * Database;
	             *
	             */
	            $this->_mng = new \MongoDB\Driver\Manager($connection_string, array($config['username'] , $config['password'] ));
	            $this->__connected = true;

	            return $this->__connected;
	        }
	        catch (MongoDB\Driver\Exception\Exception $e) {

	            /**
	             * Get the name of the file which called the script;
	             *
	             */
	            $filename = basename(__FILE__);

	            /**
	             * Echo back the error results of the
	             * connection to MongoDB;
	             */
	                echo "The $filename script has experienced an error.\n";
	                echo "It failed with the following exception:\n";

	                echo "Exception:", $e->getMessage(), "\n";
	                echo "In file:", $e->getFile(), "\n";
	                echo "On line:", $e->getLine(), "\n";
	        }
	    }

	    public function getConnection(){
	        return $this->__connection;
	    }

	    /**
	     * @param $query
	     */
	    public function interact($query){
	        switch($query['type']){
	            case 'insert' :
	                try{
	                    $data = $query['data'];
	                    $ins = new \MongoDB\Driver\BulkWrite;

	                    /**
	                     * Create an Insert for each piece of data;
	                     *
	                     */

	                    foreach($data as $data_row)
	                    {
	                        $ins->insert($data_row);
	                    }

	                    /**
	                     * Execute the insert;
	                     *
	                     */

	                    $this->_mng->executeBulkWrite($this->config['dbname'].'.'.$query['collection'], $ins);
	                }
	                catch (\MongoDB\Driver\Exception\Exception $e) {

	                    /**
	                     * Get the name of the file which called the script;
	                     *
	                     */
	                    $filename = basename(__FILE__);

	                    /**
	                     * Echo back the error results of the
	                     * connection to MongoDB;
	                     */
	                    echo "The $filename script has experienced an error.\n";
	                    echo "It failed with the following exception:\n";

	                    echo "Exception:", $e->getMessage(), "\n";
	                    echo "In file:", $e->getFile(), "\n";
	                    echo "On line:", $e->getLine(), "\n";
	                }
	            break;
	            case 'remove':
	                try{
	                    $data = $query['data'];
	                    $del = new \MongoDB\Driver\BulkWrite;

	                    /**
	                     * Apply the Remove Query array to the
	                     * BulkWrite variable;
	                     *
	                     */
	                        $del->delete($data);

	                    /**
	                     * Execute the Remove;
	                     *
	                     */

	                    $this->_mng->executeBulkWrite($this->config['dbname'].'.'.$query['collection'], $del);
	                }
	                catch (\MongoDB\Driver\Exception\Exception $e) {

	                    /**
	                     * Get the name of the file which called the script;
	                     *
	                     */
	                    $filename = basename(__FILE__);

	                    /**
	                     * Echo back the error results of the
	                     * connection to MongoDB;
	                     */
	                    echo "The $filename script has experienced an error.\n";
	                    echo "It failed with the following exception:\n";

	                    echo "Exception:", $e->getMessage(), "\n";
	                    echo "In file:", $e->getFile(), "\n";
	                    echo "On line:", $e->getLine(), "\n";
	                }
	            break;
	            case 'update':
		            try {
	                    $data = $query['data'];
	                    $options = $query['options'];
	                    //print_r($data);
	                    $condition = $query['condition'];
	                    $updt = new \MongoDB\Driver\BulkWrite;

	                    /**
	                     * Apply the Update Query array to the
	                     * BulkWrite variable;
	                     *
	                     */
	                    $updt->update($condition, $data, $options);

	                    /**
	                     * Execute the Update;
	                     *
	                     */

	                    $this->_mng->executeBulkWrite($this->config['dbname'].'.'.$query['collection'], $updt);
		            }
					catch (\MongoDB\Driver\Exception\Exception $e) {

	                    /**
	                     * Get the name of the file which called the script;
	                     *
	                     */
	                    $filename = basename(__FILE__);

	                    /**
	                     * Echo back the error results of the
	                     * connection to MongoDB;
	                     */
	                    echo "The $filename script has experienced an error.\n";
	                    echo "It failed with the following exception:\n";

	                    echo "Exception:", $e->getMessage(), "\n";
	                    echo "In file:", $e->getFile(), "\n";
	                    echo "On line:", $e->getLine(), "\n";
	                }
	            break;
	        }
	    }

	    public function queryAll($collection){
	        try {

	            $query = new \MongoDB\Driver\Query([]);

	            $rows = $this->_mng->executeQuery($this->config['dbname'].'.'.$collection, $query);

	            $data = array();
	            foreach ( $rows as $row )
	            {
	                $data[] = $row;
	            }

	            return $data;

	        } catch (\MongoDB\Driver\Exception\Exception $e) {

	            $filename = basename(__FILE__);

	            echo "The $filename script has experienced an error.\n";
	            echo "It failed with the following exception:\n";

	            echo "Exception:", $e->getMessage(), "\n";
	            echo "In file:", $e->getFile(), "\n";
	            echo "On line:", $e->getLine(), "\n";
	        }
	    }

	    public function query($collection, $filter, $options){
	        try {
	            $query = new \MongoDB\Driver\Query($filter,$options);
	            $rows = $this->_mng->executeQuery($this->config['dbname'].'.'.$collection, $query);

	            $data = array();
	            foreach ( $rows as $row ) {
	                $data[] = $row;
	            }

	            return $data;

	        } catch (\MongoDB\Driver\Exception\Exception $e) {

	            $filename = basename(__FILE__);

	            echo "The $filename script has experienced an error.\n";
	            echo "It failed with the following exception:\n";

	            echo "Exception:", $e->getMessage(), "\n";
	            echo "In file:", $e->getFile(), "\n";
	            echo "On line:", $e->getLine(), "\n";
	        }
	    }

	    public function truncate($collection){
	        try {
	           		
	               $this->_mng->executeCommand($this->config['dbname'], new \MongoDB\Driver\Command(["drop" => $collection]));

	        } catch (\MongoDB\Driver\Exception\Exception $e) {

	            $filename = basename(__FILE__);

	            echo "The $filename script has experienced an error.\n";
	            echo "It failed with the following exception:\n";

	            echo "Exception:", $e->getMessage(), "\n";
	            echo "In file:", $e->getFile(), "\n";
	            echo "On line:", $e->getLine(), "\n";
	        }
	    }

	    public function aggregate($command){
	        try {
	           		
	            $rawRespose = $this->_mng->executeCommand($this->config['dbname'], new \MongoDB\Driver\Command($command));
	            return $rawRespose;

	        } catch (\MongoDB\Driver\Exception\Exception $e) {

	            $filename = basename(__FILE__);

	            echo "The $filename script has experienced an error.\n";
	            echo "It failed with the following exception:\n";

	            echo "Exception:", $e->getMessage(), "\n";
	            echo "In file:", $e->getFile(), "\n";
	            echo "On line:", $e->getLine(), "\n";
	        }
	    }



	}

}
