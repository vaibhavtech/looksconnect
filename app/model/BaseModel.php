<?php

namespace app\model;

 use app\service\MongoDb;

class BaseModel 
{
    public static $BLOGER_COLLECTION = "bloger_form";
    protected $mongo = null;

    public function __construct(){
        $this->mongo = new MongoDb();
    }

    public function getNextSequence(){
            $filter = [];
            $options = ['sort'=>[
                '_id' => -1,
            ],
                'limit' => 1,
            ];
            $formdata = $this->mongo->query(self::$BLOGER_COLLECTION, $filter, $options);
            return ($formdata[0]->form_id + 1);
        }

    public function submitData($formData){

        $form_id = self::getNextSequence();
            $this->mongo->interact([
                'type' => 'insert',
                'data' => [
                    [
                        'form_id' => (int)$form_id,
                        'form_data' => $formData,
                        'created_at' => time(),
                    ],
                ],
                'collection' => self::$BLOGER_COLLECTION,
            ]);
            return true;
        }



}

?>