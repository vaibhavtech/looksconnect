<?php

namespace app\controller {

    use \app\model\BaseModel;
	
	class BaseController extends AbstractController
    {

        public $basemodel;

    	/**
         * @Description - Json Api
         *
         * @RequestMapping(url="api/formdata/submit",method="POST",type="json")
         * @RequestParams(true)
         */
        public function formPostData($formData)
        {
        	if(is_array($formData) && !empty($formData)){
        		 $this->basemodel = new BaseModel();
                 return $this->basemodel->submitData($formData);
        	}
        }





    }

}